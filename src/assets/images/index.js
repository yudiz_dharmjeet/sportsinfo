import DeleteIcon from "./whitedeleteicon.png";
import BackButton from "./whitebackicon.png";
import EditButton from "./whiteediticon.png";

export { DeleteIcon, BackButton, EditButton };
