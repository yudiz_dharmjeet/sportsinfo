import React, { useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Admin, EditArticle, Home, SingleArticle } from "./pages";

function App() {
  useEffect(() => {
    localStorage.setItem("username", "admin");
    localStorage.setItem("password", "admin@123");
  }, []);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" exact element={<Home />} />
        <Route path="/admin" element={<Admin />} />
        <Route path="/article/:id" element={<SingleArticle />} />
        <Route path="/edit/article/:id" element={<EditArticle />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
