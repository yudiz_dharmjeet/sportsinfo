import Admin from "./Admin/Admin";
import Home from "./Home/Home";
import SingleArticle from "./SingleArticle/SingleArticle";
import EditArticle from "./EditArticle/EditArticle";

export { Admin, Home, SingleArticle, EditArticle };
