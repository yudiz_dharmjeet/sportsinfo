import React from "react";
import { useNavigate } from "react-router-dom";
import { ArticleList } from "../../components";

import "./Home.scss";

function Home() {
  const navigate = useNavigate();

  function handleLogout() {
    localStorage.setItem("login", JSON.parse(false));
    navigate("/");
  }

  return (
    <main className="home">
      {localStorage.getItem("login") == "true" && (
        <button className="logoutbtn" onClick={handleLogout}>
          Logout
        </button>
      )}
      <ArticleList />
    </main>
  );
}

export default Home;
