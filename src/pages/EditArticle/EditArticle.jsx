import React, { useEffect, useRef } from "react";
import { useParams } from "react-router-dom";
import { ArticleState } from "../../context/Context";
import { useNavigate } from "react-router-dom";

import "./EditArticle.scss";

function EditArticle() {
  const titlee = useRef(null);
  const descriptionn = useRef(null);

  const params = useParams();

  const navigate = useNavigate();

  const { state, dispatch } = ArticleState();

  const myArticle = state.articles.find((article) => article._id == params.id);

  function handleSubmit(e) {
    e.preventDefault();
    dispatch({
      type: "EDIT_ARTICLE",
      payload: {
        id: params.id,
        title: titlee.current.value,
        desc: descriptionn.current.value,
      },
    });
    navigate("/");
  }

  useEffect(() => {
    titlee.current.focus();
    if (localStorage.getItem("login") == "false") {
      navigate("/");
    }
  }, []);

  return (
    <div className="editpage">
      <h1>Edit Article</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="title"
          id="title"
          placeholder="Title"
          ref={titlee}
          defaultValue={myArticle.sTitle}
        />
        <textarea
          name="description"
          id="description"
          placeholder="Description"
          ref={descriptionn}
          defaultValue={myArticle.sDescription}
        />
        <button>Submit</button>
      </form>
    </div>
  );
}

export default EditArticle;
