import React, { useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";

import "./Admin.scss";

function Admin() {
  const username = useRef(null);
  const password = useRef(null);

  const navigate = useNavigate();

  function handleSubmit(e) {
    e.preventDefault();
    if (
      (localStorage.getItem("username") == username.current.value) &
      (localStorage.getItem("password") == password.current.value)
    ) {
      localStorage.setItem("login", "true");
      navigate("/");
    } else {
      alert("Please enter correct value");
    }
  }

  useEffect(() => {
    username.current.focus();
    if (localStorage.getItem("login") == "true") {
      navigate("/");
    }
  }, []);

  return (
    <div className="admin">
      <h1>Admin Login</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          name="username"
          id="username"
          placeholder="Username"
          ref={username}
        />
        <input
          type="password"
          name="password"
          id="password"
          placeholder="Password"
          ref={password}
        />
        <button>Login</button>
      </form>
    </div>
  );
}

export default Admin;
