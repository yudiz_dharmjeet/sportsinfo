import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import { BackButton } from "../../assets/images";
import { ArticleState } from "../../context/Context";

import "./SingleArticle.scss";

function SingleArticle() {
  let param = useParams();
  let id = param.id;

  const navigate = useNavigate();

  const { state } = ArticleState();

  const myArticle = state.articles.find((article) => article._id == id);

  function handleBackButton() {
    navigate("/");
  }

  return (
    <>
      <div className="editbtn" onClick={handleBackButton}>
        <img src={BackButton} alt="BackButton" />
      </div>
      <div
        className="singlepage"
        style={{ backgroundImage: `url(${myArticle.sImage})` }}
      >
        <div className="singlepage__inner">
          <div className="container">
            <div className="singlepage__inner__description">
              <h1>{myArticle.sTitle}</h1>
              <div className="article__desc-box__bottom">
                <div className="article__desc-box__bottom__left">
                  <h4>
                    {myArticle.iId.sFirstName} {myArticle.iId.sLastName}
                    <span>a year ago</span>
                  </h4>
                </div>
                <div className="article__desc-box__bottom__right">
                  <div className="article__desc-box__bottom__right__single">
                    <img
                      src="https://www.sports.info/comment-icon.7aef209a3b2086028430.svg"
                      alt="Comment"
                    />
                    <p>{myArticle.nCommentsCount}</p>
                  </div>
                  <div className="article__desc-box__bottom__right__single">
                    <img
                      src="https://www.sports.info/view-icon.b16661e96527947b18f1.svg"
                      alt="View"
                    />
                    <p>{myArticle.nViewCounts}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="singlepage__description">
        <div className="container">
          <p>{myArticle.sDescription}</p>
        </div>
      </div>
    </>
  );
}

export default SingleArticle;
