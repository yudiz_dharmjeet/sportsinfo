export const reducers = (state, action) => {
  switch (action.type) {
    case "REMOVE_ARTICLE":
      return {
        ...state,
        articles: [
          ...state.articles.filter((article) => article._id != action.payload),
        ],
      };

    case "EDIT_ARTICLE":
      return {
        ...state,
        articles: [
          ...state.articles.map((article) => {
            if (article._id == action.payload.id) {
              return {
                ...article,
                sTitle: action.payload.title,
                sDescription: action.payload.desc,
              };
            } else {
              return article;
            }
          }),
        ],
      };

    default:
      return state;
  }
};
