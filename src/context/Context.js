import React, { createContext, useContext, useReducer } from "react";

import PropTypes from "prop-types";

import Articles from "../data/sports-info.json";
import { reducers } from "./Reducers";

const ArticleContext = createContext();

function Context({ children }) {
  const [state, dispatch] = useReducer(reducers, {
    articles: Articles.data,
  });

  return (
    <ArticleContext.Provider value={{ state, dispatch }}>
      {children}
    </ArticleContext.Provider>
  );
}

Context.propTypes = {
  children: PropTypes.node,
};

export default Context;

export const ArticleState = () => {
  return useContext(ArticleContext);
};
