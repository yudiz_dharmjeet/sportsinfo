import React from "react";

import PropTypes from "prop-types";

import "./ArticleCard.scss";
import { useNavigate } from "react-router-dom";
import { DeleteIcon, EditButton } from "../../assets/images";
import { ArticleState } from "../../context/Context";

function ArticleCard({ article }) {
  let navigate = useNavigate();

  const { dispatch } = ArticleState();

  function handleArticleClick() {
    navigate(`/article/${article._id}`);
  }

  function handleDeleteArticle() {
    if (
      confirm(
        `Are you sure you want to delete : ${article.sTitle} , this article ?`
      )
    ) {
      dispatch({ type: "REMOVE_ARTICLE", payload: article._id });
    }
  }

  function handleEditArticle() {
    navigate(`edit/article/${article._id}`);
  }

  let createdAt = new Date(article.dCreatedAt);
  let dCreatedAt =
    createdAt.getFullYear() == 2021 ? "a year ago" : createdAt.getFullYear();

  return (
    <div className="article">
      <div className="deletebtn" onClick={handleDeleteArticle}>
        {localStorage.getItem("login") == "true" && (
          <img src={DeleteIcon} alt="deleteicon" />
        )}
      </div>
      <div className="editbtnnn" onClick={handleEditArticle}>
        {localStorage.getItem("login") == "true" && (
          <img src={EditButton} alt="editbutton" />
        )}
      </div>
      <div className="article__img-box">
        <img
          onClick={handleArticleClick}
          src={article.sImage}
          alt={article.sTitle}
        />
      </div>
      <div className="article__desc-box">
        <div className="article__desc-box__top">
          <h3 onClick={handleArticleClick}>{article.sTitle}</h3>
          <p>{article.sDescription.substring(0, 200)}...</p>
        </div>
        <div className="article__desc-box__bottom">
          <div className="article__desc-box__bottom__left">
            <h4 onClick={handleArticleClick}>
              {article.iId.sFirstName} {article.iId.sLastName}
              <span>{dCreatedAt}</span>
            </h4>
          </div>
          <div className="article__desc-box__bottom__right">
            <div className="article__desc-box__bottom__right__single">
              <img
                src="https://www.sports.info/comment-icon.7aef209a3b2086028430.svg"
                alt="Comment"
              />
              <p>{article.nCommentsCount}</p>
            </div>
            <div className="article__desc-box__bottom__right__single">
              <img
                src="https://www.sports.info/view-icon.b16661e96527947b18f1.svg"
                alt="View"
              />
              <p>{article.nViewCounts}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

ArticleCard.propTypes = {
  article: PropTypes.object,
};

export default ArticleCard;
