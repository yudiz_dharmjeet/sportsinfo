import React from "react";
import { ArticleState } from "../../context/Context";
import ArticleCard from "../ArticleCard/ArticleCard";

import "./ArticleList.scss";

function ArticleList() {
  const { state } = ArticleState();

  return (
    <div className="article-list">
      {state.articles.map((article) => {
        return <ArticleCard key={article._id} article={article} />;
      })}
    </div>
  );
}

export default ArticleList;
